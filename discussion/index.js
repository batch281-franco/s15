// comments single line comment
// comments are parts of the code that gets ignored by the language
// comments are meant to describe the written code
/* two types of comments 
1. single line  
2. multi line
*/ 

console.log("hello world");




//  variables

/* - it is used to contain data 
*/

// declaring variables - tells our devices that a variable name is created and is 
// ready to store data.

/*
syntax :
	let / const variableName;
	let - keyword usually used in declaring a variable
	const - keyword usually used in declaring CONSTANT variable.
	
*/

let myVariable;
console.log(myVariable);
// console.log(hello);

// initializing variables - the instance when a variable is given it's initial/starting value.
/*
	syntax:
		let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest =3.539;

// reassigning variable value
// syntax: variableName = newValue

productName ='laptop'
console.log(productName);

// interest = 4.489;

/* declare a variable first*/
let supplier;
// initialization of value
supplier = 'john smith trading';
// multiple variable declaration
let productCode = 'DC017', productBrand = 'Dell';
console.log(productCode,productBrand);

// Data types 
// string - handles a series of characters that create a word, phrase, or a sentence.
let country = 'philippines';
let province = 'metro manila';

// concatenating strings ( + is used)
let fullAddress = province + ', ' +country;
console.log(fullAddress);

let greeting = 'I live in the ' + country
console.log(greeting);

// escape character (\) in strings is combination with other characters can produce a different effect (\n)
let mailAddress = 'metro manila\n\nPhilippines';
console.log(mailAddress);

let message = "john's employees went home early";
console.log(message);

// numbers 
//  integers/whole numbers
let headcount = 32;
console.log(headcount);

// decimal numbers/fractions
let grade = 98.7;
console.log(grade);

// exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// combining text/numbers and strings
console.log("john's grade last quarter is " + grade);
// boolean - used to store vAlue relating to the state of certain things

let isMarried = true;
let inGoodConduct = false;
console.log("Married: "+ isMarried);
console.log("GoodConduct: "+ inGoodConduct);

// array - are special kind of data type that's used to store multiple values.
// syntax: let/const arrayName = [elementA, elementB....]
let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// objects - another special kind of data type that's used to mimic real world objects/items.
/*
	syntax
		let/const objectName = {
			property : value, 
			propertyB : value
		}
*/	
let person = {
	fullName : 'Juan Dela Cruz',
	age : 35,
	isMarried : false,
	contact : ["0917 123 4567", "6546 2345"],
	address: {
		houseNumber : '123',
		city: 'manila'
	}
};
console.log(person);

let myGrades = {
	firstGrading:98.7,
	secondGrading:92.1,
	thirdGrading:94.6,
	fourthGrading: 92.7
}
console.log(myGrades);

// type operator - used to determing the type of data or the value of the variable

console.log(typeof myGrades);
console.log(typeof grades);

// null - used to intentionally express the absence of a value in a variable declaration/initialization

let spouse = null;
let myNumber = 0;
let myString = '';

// undefined - 
let fullName;
console.log(fullName);